;;; gk-ibuffer.el --- Ibuffer setup.                 -*- lexical-binding: t; -*-

;;; Commentary:

;;; URL: https://www.reddit.com/r/emacs/comments/6qfvia/can_we_talk_about_managingswitching_between_many/

;;; Code:
(require 'ibuffer)
(require 'ibuffer-vc)

;;; IBuffer:
;; Use human readable Size column instead of original one
(define-ibuffer-column size-h
  (:name "Size" :inline t)
  (let ((bs (buffer-size)))
    (cond ((> bs 1e6) (format "%7.1fmB" (/ bs 1e6)))
          ((> bs 1e3) (format "%7.1fkB" (/ bs 1e3)))
          (t          (format "%7d  " bs)))))

(setf ibuffer-formats
      '((mark modified read-only vc-status-mini " "
              (name 18 18 :left :elide)
              " "
              (size-h 9 -1 :right)
              " "
              (mode 16 16 :left :elide)
              " "
              (vc-status 16 16 :left)
              " "
              filename-and-process))
      ibuffer-show-empty-filter-groups nil)

(define-ibuffer-filter name-not
    "Negated buffer name match."
  (:description "buffer name not"
   :reader (read-from-minibuffer "Exclude buffers (regexp): "))
  (not (string-match qualifier (buffer-name buf))))

(defvar gk-ibuffer-filters
  '(("Emacs"
     (name . "^\\*\\(scratch\\|Messages\\)\\*$"))
    ("VC"
     (name . "^\\*\\(vc\\|log\\)-?"))
    ("Documentation"
     (name . "^\\*\\(Help\\|info\\|Man [0-9]?\\)"))
    ("Special buffers"
     (name-not . "compilation")
     (name . "^\\*.*\\*$"))
    ("EWW Reading"
     (mode . eww-mode)))
  "Fixed filter definitions for ibuffer.")

(cl-defun gk-ibuffer-generate-filter-groups-by-dir ()
  "Create a set of ibuffer filter groups based on the dirs of buffers."
  (let* ((func (lambda (buf)
                 (when-let ((bufnam (buffer-file-name buf)))
                   (if-let ((linknam (file-symlink-p bufnam)))
                       (file-name-directory (expand-file-name linknam))
                     (file-name-directory (expand-file-name bufnam))))))
         (dirs (ibuffer-remove-duplicates (delq nil (mapcar func (buffer-list))))))
    (mapcar (lambda (dir) (cons (concat "Directory: " dir) `((dir . ,dir)))) dirs)))

(define-ibuffer-filter dir
    "Toggle current view to buffers with dir QUALIFIER."
  (:description "directory" :reader (read-from-minibuffer "Filter by dir (regexp): "))
  (ibuffer-awhen (buffer-file-name buf)
    (string= qualifier (file-name-directory it))))

(define-advice ibuffer-update (:before (&rest args) autogroups)
  "Group related buffers together using \u2018ibuffer-vc\u2019 and \u2018dir\u2019,
and special ones sepatarely."
  (ignore args)
  (setf ibuffer-filter-groups
        (append
         gk-ibuffer-filters
         (ibuffer-vc-generate-filter-groups-by-vc-root)
         (gk-ibuffer-generate-filter-groups-by-dir))))

;; Hide these buffers by default.
(defvar gk-ibuffer-collapsed-groups (list "Special buffers" "Emacs" "Documentation"
                                          "VC"))

(define-advice ibuffer (:after (&rest args) gk-hidden-groups)
  "Hide groups in \u2018gk-ibuffer-collapsed-groups\u2019."
  (ignore args)
  (save-excursion
    (dolist (group gk-ibuffer-collapsed-groups)
      (ignore-errors
        (ibuffer-jump-to-filter-group group)
        (ibuffer-toggle-filter-group)))))

(cl-defun gk-ibuffer-hook ()
  (unless (eq ibuffer-sorting-mode 'alphabetic)
    (ibuffer-do-sort-by-filename/process))
  (ibuffer-update nil t))

(add-hook 'ibuffer-hook 'gk-ibuffer-hook)

(provide 'gk-ibuffer)
;;; gk-ibuffer.el ends here
