(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#D8DEE9" "#ff6c6b" "#98be65" "#ECBE7B" "#51afef" "#c678dd" "#46D9FF" "#bbc2cf"])
 '(bmkp-last-as-first-bookmark-file "/home/bhepple/.emacs.d/.local/etc/bookmarks")
 '(custom-safe-themes
   (quote
    ("f7216d3573e1bd2a2b47a2331f368b45e7b5182ddbe396d02b964b1ea5c5dc27" "e074be1c799b509f52870ee596a5977b519f6d269455b84ed998666cf6fc802a" "4bca89c1004e24981c840d3a32755bf859a6910c65b829d9441814000cf6c3d0" "81fbf1e7775ba10361f9d744aeb5360cf29606524192c7fc55498e1900380bed" "9d0d783a5f0572a9b90405a91dd24ea8400c044d5157cd9bb80e34d20b8a0abf" "37144b437478e4c235824f0e94afa740ee2c7d16952e69ac3c5ed4352209eefb" default)))
 '(fci-rule-color "#5B6268")
 '(jdee-db-active-breakpoint-face-colors (cons "#1B2229" "#51afef"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#1B2229" "#98be65"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#1B2229" "#3f444a"))
 '(objed-cursor-color "#ff6c6b")
 '(pdf-view-midnight-colors (cons "#bbc2cf" "#282c34"))
 '(persp-mode t nil (persp-mode))
 '(rustic-ansi-faces
   ["#282c34" "#ff6c6b" "#98be65" "#ECBE7B" "#51afef" "#c678dd" "#46D9FF" "#bbc2cf"])
 '(safe-local-variable-values
   (quote
    ((eval outline-hide-sublevels 4)
     (eval setq-default indent-tabs-mode nil)
     (eval setq compile-command "./gjots2 test.gjots")
     (eval add-hook
           (make-local-variable
            (quote after-save-hook))
           (quote bh:ensure-in-vc-or-check-in)
           t)
     (eval setq python-indent 4)
     (eval setq tab-width 4)
     (eval setq indent-tabs-mode nil)
     (eval setq compile-command "cd ..; ./gjots2 test.gjots"))))
 '(vc-annotate-background "#282c34")
 '(vc-annotate-color-map
   (list
    (cons 20 "#98be65")
    (cons 40 "#b4be6c")
    (cons 60 "#d0be73")
    (cons 80 "#ECBE7B")
    (cons 100 "#e6ab6a")
    (cons 120 "#e09859")
    (cons 140 "#da8548")
    (cons 160 "#d38079")
    (cons 180 "#cc7cab")
    (cons 200 "#c678dd")
    (cons 220 "#d974b7")
    (cons 240 "#ec7091")
    (cons 260 "#ff6c6b")
    (cons 280 "#cf6162")
    (cons 300 "#9f585a")
    (cons 320 "#6f4e52")
    (cons 340 "#5B6268")
    (cons 360 "#5B6268")))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
