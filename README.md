# dotfiles

Some dotfiles that I use.

doom emacs files are:

- .doom.d/*
- .config/emacs/*

sway config files are:

- .config/sway/config
- .config/i3blocks/config
- .config/i3blocks/i3blocks/*

sway and other scripts are here:

- bin/*