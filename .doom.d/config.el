;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Bob Hepple"
      user-mail-address "bob.hepple@gmail.com")

      ;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
      ;; are the three important ones:
      ;;
      ;; + `doom-font'
      ;; + `doom-variable-pitch-font'
      ;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
      ;;   presentations or streaming.
      ;;
      ;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
      ;; font string. You generally only need these two:
      ;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
      ;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

      ;; There are two ways to load a theme. Both assume the theme is installed and
      ;; available. You can either set `doom-theme' or manually load a theme with the
      ;; `load-theme' function. This is the default:
      (setq doom-theme 'doom-one)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Sync/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; BH: Some notes on the rest of this file:

;; Use C-<f12> or C-<tab> to cycle visibility (outline-minor-mode)

; outline mode:
; C-c
; C-c @ C-a show all
; C-c @ C-e show entry
; C-c @ C-k show branches
; C-c @ C-q hide sublevels - just the top level (go to a top level first)
; C-c @ C-t top level structure - ie hide everything but the body
; C-c @ C-d hide subtree
; C-c @ TAB show children
; Most useful: C-f12 cycle visibility

;;;* Prelims

;;;;* stupid windows box:
(when (getenv "OS")
  (when (string-match "Windows.*" (getenv "OS"))
    (eval-when-compile (setq locate-command "/home/mobaxterm/bin/mylocate"))
    (setq select-enable-clipboard "true")
    (set-background-color "black")
    (set-foreground-color "white")))

;;;* local elisp
; for elisp not in MELPA:
(setq load-path (append load-path '("~/.config/emacs")))

;;;* Languages
;;;** C and C++ mode stuff

; this is common for c and c++. Maybe java?

;; From the info page and stroustrup style and edited:
(defconst bh:c-style
  '((c-tab-always-indent        . t)
    (c-comment-only-line-offset . 0)
    (c-hanging-braces-alist     . ((substatement-open before after)
                                   (brace-list-open)))
    (c-hanging-colons-alist     . ((member-init-intro before)
                                   (inher-intro)
                                   (case-label after)
                                   (label after)
                                   (access-label after)))
;   (c-cleanup-list             . (scope-operator
;                                  empty-defun-braces
;                                  defun-close-semi))
    (c-offsets-alist            . ((statement-block-intro . +)
                                   (substatement-open . 0)
                                   (label . 0)
                                   (statement-cont . +)
                                   (case-label . 8)))
;;  (c-offsets-alist            . ((arglist-close . c-lineup-arglist)
;;                                 (substatement-open . 0)
;;                                 (case-label        . 4)
;;                                 (block-open        . 0)
;;                                 (knr-argdecl-intro . -)))
    (c-echo-syntactic-information-p . t)
    (indent-tabs-mode . nil)
    )
  "My C Programming Style"
  )

(defun bh:c-mode-fiddle ()
  (c-add-style "PERSONAL" bh:c-style t)
  (eval-when-compile (setq c-auto-newline nil)) ; auto-add a newline, yuck!
  (c-toggle-auto-newline 0) ; turn off auto-newline
  (setq indent-tabs-mode nil) ; use spaces for indentation
  (setq tab-width 4)
  (hs-minor-mode) ; hide/show mode
  (hs-show-all)
  (define-key hs-minor-mode-map (kbd "C-<tab>") 'hs-toggle-hiding)
  (define-key hs-minor-mode-map (kbd "C-<f12>") 'hs-toggle-hiding)
;  (define-key c-mode-map (kbd "C-<return>") 'c-mark-function)
;  (define-key c-mode-base-map (kbd "C-m") 'c-context-line-break) ; make RET act like ^J!! and indent
;  (c-set-offset 'arglist-cont-nonempty 'c-lineup-cont) ; for RSA
  (font-lock-mode)

;(c-set-style "stroustrup")
;(setq c-tab-always-indent nil) ; Only indent if cursor is at left side
;; for auto-newline _and_ hungry-delete:
;; (c-toggle-auto-hungry-state 1)

;  (setq c-indent-level indent)
;  (setq c-basic-offset indent)
;  (setq c-continued-statement-offset indent)
;  (setq c-brace-imaginary-offset 0)
;  (setq c-continued-brace-offset (* -1 indent))
;  (setq c-brace-offset 0)
;  (setq c-argdecl-indent indent)
;  (setq c-label-offset (* -1 indent))
  )

;;(setq auto-mode-alist (cons '("\\.C$" . c++-mode) auto-mode-alist))
;;(setq auto-mode-alist (cons '("\\.cc$" . c++-mode) auto-mode-alist))
(add-hook 'c-mode-common-hook 'bh:c-mode-fiddle)

; This is the example from the info page:
(defun c-lineup-streamop (langelem)
;; lineup stream operators
  (save-excursion
    (let* ((relpos (cdr langelem))
           (curcol (progn (goto-char relpos)
                          (current-column))))
      (re-search-forward "<<\\|>>" (c-point 'eol) 'move)
      (goto-char (match-beginning 0))
      (- (current-column) curcol))))

;(defun c-lineup-cont (langelem)
;  ;; lineup continuation lines simply by adding the offset - but from
;  ;; the start of the prior line rather than syntactically - for RSA!!
;  (save-excursion
;   (let* ((relpos (cdr langelem))
;          (curcol (progn (goto-char relpos)
;                         (current-column))))
;     (goto-char (c-point 'bol))
;     (re-search-forward "[^    ]" (c-point 'eol) 'move)
;     (goto-char (match-beginning 0))
;     (+ c-basic-offset (- (current-column) curcol)))))

;;
;; Set Stroustrup C/C++/Java coding style
;;
(add-hook 'c-mode-hook
          '(lambda ()
             (c-set-style "Stroustrup")
             (c-toggle-auto-state -1)
             (setq c-electic-flag nil)))

(add-hook 'c++-mode-hook
          '(lambda ()
             (c-set-style "Stroustrup")
             (c-toggle-auto-state -1)
             (setq c-electic-flag nil)))

(add-hook 'awk-mode-hook
          '(lambda ()
             (c-set-style "Stroustrup")
             (c-toggle-auto-state -1)
             (setq c-electic-flag nil)))

(add-hook 'java-mode-hook
          '(lambda ()
             (c-toggle-auto-state -1)
             (c-set-style "Stroustrup")))

;-------------------------------------
;;;** shell-script-mode:

(defun bh:sh-indent-rule-for-close-brace ()
  (when (eq ?= (char-before))
    (skip-chars-backward "[:alnum:]_=")
    (current-column)))
;(add-hook 'sh-mode-hook
;          (lambda ()
;            (add-hook 'smie-indent-functions
;                      #'bh:sh-indent-rule-for-close-brace
;                      nil 'local)))

(defun bh:sh-indent-rules ()
  (setq sh-use-smie nil)
  (setq sh-styles-alist
        '(("bobs"
           (sh-basic-offset . 4)
           (sh-first-lines-indent . 0)
           (sh-indent-after-case . +)
           (sh-indent-after-do . +)
           (sh-indent-after-done . 0)
           (sh-indent-after-else . +)
           (sh-indent-after-if . +)
           (sh-indent-after-loop-construct . +)
           (sh-indent-after-open . +)
           (sh-indent-comment . t)
           (sh-indent-for-case-alt . ++)
           (sh-indent-for-case-label . +)
           (sh-indent-for-continuation . +)
           (sh-indent-for-do . 0)
           (sh-indent-for-done . 0)
           (sh-indent-for-else . 0)
           (sh-indent-for-fi . 0)
           (sh-indent-for-then . 0))))
  (sh-load-style "bobs"))
(add-hook 'sh-mode-hook #'bh:sh-indent-rules)

(defun bh:sh-smie-rules (orig-fun kind token)
  (pcase (cons kind token)
      (`(:before . "|") nil)
    (_ (funcall orig-fun kind token))))
(advice-add 'sh-smie-sh-rules :around #'bh:sh-smie-rules)

(defun bh:sh-mode-customisations ()
  (hs-minor-mode)
  (hs-show-all)
  (define-key hs-minor-mode-map (kbd "C-<tab>") 'hs-toggle-hiding)
  (define-key hs-minor-mode-map (kbd "C-<f12>") 'hs-toggle-hiding))
(add-hook 'sh-mode-hook #'bh:sh-mode-customisations)

;;;** nroff etc
;; Set appropriate flags for nroff documents
(defun bh:nroff-mode-hook ()
  (eval-when-compile (setq ispell-filter-hook-args '("-w")))
  (eval-when-compile (setq ispell-filter-hook "deroff"))
  (eval-when-compile (setq ispell-words-have-boundaries nil))
  (eval-when-compile (define-key nroff-mode-map (kbd "C-b") '(lambda ()
                                            (interactive)
                                            (insert "\\fB\\fP")
                                            (backward-char 3))))
  (eval-when-compile (define-key nroff-mode-map (kbd "C-i") '(lambda ()
                                                               (interactive)
                                                               (insert "\\fI\\fP")
                                                               (backward-char 3)))))
(add-hook 'nroff-mode-hook 'bh:nroff-mode-hook)
(setq auto-mode-alist (cons '("\\.1$" . nroff-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.1m$" . nroff-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.me$" . nroff-mode) auto-mode-alist))

(autoload 'man-preview "man-preview" nil t)
(add-hook 'nroff-mode-hook
          (lambda ()
            (eval-when-compile (define-key nroff-mode-map [f8] 'man-preview))))

;;;** lisp
(add-hook 'lisp-mode-hook (lambda() (font-lock-mode t)))
;;;** python
;(use-package indent-tools :ensure) ; installed as package

(add-hook 'python-mode-hook (lambda() (font-lock-mode t)))
;; from /usr/share/emacs/site-lisp/50python-mode-gentoo.el:
; ohai? (setq load-path (cons "/usr/share/emacs/site-lisp/python-mode" load-path))
; ohai? (setq auto-mode-alist
; ohai?       (cons '("\\.py$" . python-mode) auto-mode-alist))
; ohai? (setq interpreter-mode-alist
; ohai?       (cons '("python" . python-mode)
; ohai?             interpreter-mode-alist))
; ohai? (autoload 'python-mode "python" "Python editing mode." t)

;;;** Java
;;(autoload 'java-mode "java-cc-mode" "Java Editing Mode" t)
;;(setq auto-mode-alist (append '(("\\.java$" . java-mode))
;;                (if (boundp 'auto-mode-alist) auto-mode-alist)))
;;(defun my-java-mode-hook ()
;;  ;; my customizations for java-cc-mode
;;  (c-set-offset 'inher-cont '+)
;;  (define-key java-mode-map [(meta backspace)] 'backward-kill-word))
;;(add-hook 'java-mode-hook 'my-java-mode-hook)
;;;** tcl
(autoload 'tcl-mode "tcl" "Tcl mode." t)
(autoload 'inferior-tcl "tcl" "Run inferior Tcl process." t)
(setq auto-mode-alist (append '(("\\.tcl$" . tcl-mode)) auto-mode-alist))
; (setq tcl-help-directory "/usr/local/lib/tclX/7.3a/help")
; (autoload 'tcl-help-on-word "tcl" "Help on Tcl commands" t)

;;;* Useful functions

(defun bh:just-tab ()
  "Just put a fecking tab in, for chrisake!"
  (interactive)
  (insert "\t"))

(defun bh:kill-next-window ()
  "As it says - mainly for when we're full screen"
  (interactive)
  (other-window 1)
  (delete-window))

(defun bh:sudo-find-file (file-name) "Like find file, but opens the file as root."
       (interactive "FSudo Find File: ")
       (let ((tramp-file-name (concat "/sudo::" (expand-file-name file-name))))
         (find-file tramp-file-name)))

(defun bh:paren-match ()
  "Jumps to the paren matching the one under point, and does nothing if there isn't one."
  (interactive)
  (cond ((looking-at "[\(\[{]")
         (forward-sexp 1)
         (backward-char))
        ((looking-at "[])}]")
         (forward-char)
         (backward-sexp 1))
        (t (message "Could not find matching paren."))))

(defun bh:toggle-case-fold ()
  "Toggle the variable case-fold-search."
  (interactive)
  (if (equal case-fold-search nil)
      (progn
        (setq case-fold-search t)
        (message "case-fold-search set to TRUE"))
    (progn
      (setq case-fold-search nil)
      (message "case-fold-search set to nil"))))

(defun bh:toggle-tab-width ()
  "Toggle tab-width between 4 and 8."
  (interactive)
  (if (equal tab-width 4)
      (progn
        (setq tab-width 8)
        (message "tab-width set to 8"))
    (progn
      (setq tab-width 4)
      (message "tab-width set to 4")))
  (recenter-top-bottom))

(defun bh:dup-line () "Duplicate the current line"
       (interactive)
       (beginning-of-line nil)
       (save-excursion
         (bh:yank-whole-line)
         (yank)
         (forward-line -1)))

(defun bh:kill-whole-line () "Delete the current line"
       (interactive)
       (beginning-of-line nil)
       (kill-line 1))

(defun bh:yank-whole-line () "Yank the current line"
       (interactive)
       (save-excursion
         (beginning-of-line nil)
         (let ((beg (point))) (forward-line 1) (kill-ring-save beg (point)))))

(defun bh:copy-line () "Copy the current line to kill buffer"
       (interactive)
       (beginning-of-line nil)
       (set-mark (point))
       (forward-line 1)
       (append-next-kill)
       (copy-region-as-kill (mark) (point)))

(defun bh:time-stamp () "Print a string like 20200830122439"
       (interactive)
       (insert (format-time-string "%Y%m%d%H%M%S")))

(defun bh:date-stamp () "Print s string like Sun Aug 30, 2020 12:25"
       (interactive)
       (insert (format-time-string "%a %b %e, %Y %H:%M ")))

(defun bh:insert-x-selection () ""
       (interactive)
       (insert (gui-get-selection 'PRIMARY 'STRING)))

(defun bh:insert-x-clipboard () ""
       (interactive)
       (insert (gui-get-selection 'CLIPBOARD 'STRING)))

(defun bh:pwd-to-clipboard () ""
       (interactive)
       (setq tmp (pwd))
       (if (string-match "^Directory " tmp)
           (setq tmp (replace-match "" t t tmp)))
       (gui-select-text tmp))

;;;* keybindings
;;;** Function keys
(global-set-key (kbd "<f1>")            'scroll-other-window)
(global-set-key (kbd "S-<f1>")          '(lambda () (interactive)
                                           (scroll-other-window '-)))
(global-set-key (kbd "C-<f1>")          'toggle-truncate-lines)
(global-set-key (kbd "<f2>")            'bh:toggle-case-fold)
(global-set-key (kbd "S-<f2>")          'bh:toggle-tab-width)
(global-set-key (kbd "<f3>")            'ediff-revision)
(global-set-key (kbd "C-<f3>")          'ediff-buffers)
(global-set-key (kbd "S-<f3>")          'magit-status)
(global-set-key (kbd "S-<f4>")          'bh:pwd-to-clipboard)
(global-set-key (kbd "<f4>")            '(lambda () (interactive)
                                           (bh:pwd-to-clipboard)
                                           (start-process "term" nil "mrxvt")))

;(global-set-key (kbd "<f5>")            'rgrep)
(global-set-key (kbd "<f5>")            'rg)
(global-set-key (kbd "C-<f5>")          'locate)
(global-set-key (kbd "<f6>")            'bh:kill-whole-line)
(global-set-key (kbd "C-<f6>")          'bh:yank-whole-line)
(global-set-key (kbd "C-S-<f6>")        'bh:dup-line)
(global-set-key (kbd "C-M-y")           'bh:kill-whole-line)
(global-set-key (kbd "<f7>")            'compile)
(global-set-key (kbd "S-<f7>")          'kill-compilation)
(global-set-key (kbd "C-<f7>")          'next-error)
(global-set-key (kbd "<f8>")            'bookmark-bmenu-list)
(global-set-key (kbd "C-<f8>")          'ibuffer)
(global-set-key (kbd "S-<f8>")          '(lambda () (interactive) (buffer-menu t)))
(global-set-key (kbd "<f9>")            'open-rectangle)
(global-set-key (kbd "S-<f9>")          'ispell-word)
(global-set-key (kbd "<f10>")           'kill-rectangle)
(global-set-key (kbd "S-<f10>")         'copy-rectangle-as-kill)
(global-set-key (kbd "C-<f10>")         'recentf-open-more-files)
(global-set-key (kbd "<f11>")           'yank-rectangle)
(global-set-key (kbd "C-<f11>")         'bh:date-stamp)
(global-set-key (kbd "S-<f11>")         'bh:time-stamp)
; (global-set-key (kbd "M-<f11>")       nil) let window mgr have this for volume-down on achar
; (global-set-key (kbd "S-<f12>")         'UNASSIGNED)
(global-set-key (kbd "<f12>")           'other-window)
; (global-set-key (kbd "M-<f12>")       nil) let window mgr have this for volume-up on achar

;;;** gdb stuff:
                                        ; maybe use gud-mode-hook?
                                        ;(global-set-key (kbd "M-<f1>")          'gud-next) ; C-c C-n
(global-set-key (kbd "M-<f1>")          'gdb-restore-windows)
                                        ;(global-set-key (kbd "M-<f2>")          'gud-step) ; C-c C-s
(global-set-key (kbd "M-<f2>")          'gdb-many-windows)
                                        ;(global-set-key (kbd "M-<f3>")          'gud-cont) ; C-c C-r
                                        ;(global-set-key (kbd "M-<f4>")          'gud-break); C-c C-b (C-c C-d to delete breakpoint)
                                        ;(global-set-key (kbd "M-<f5>")          'gud-run)
                                        ;(global-set-key (kbd "M-<f6>")          'gud-tbreak)
(global-set-key (kbd "M-<f7>")          'gud-up)
(global-set-key (kbd "M-<f8>")          'gud-down)
                                        ;(global-set-key (kbd "M-<f9>")          'gud-until)
                                        ;(global-set-key (kbd "M-<f10>")         'gud-finish)
                                        ;(global-set-key (kbd "M-<f11>")         '(lambda()
                                        ;                                            "Set temporary break and go"
                                        ;                                            (interactive)
                                        ;                                            (gud-tbreak t)
                                        ;                                            (gud-cont nil)))
                                        ; (global-set-key (kbd "M-<f12>")         'gud-print)

;;; ** C-x keybinds
(define-key ctl-x-map "%"                'bh:paren-match)
(define-key ctl-x-map "="                'what-line)
                                        ; (bind-key* ctl-x-map "C-b"        'buffer-menu)

(global-set-key (kbd "C-x C-b")        '(lambda (&optional arg)
                                          "runs buffer-menu but with the sense of C-u inverted (ie files only unless C-u is given)"
                                          (interactive "P")
                                          (setq arg (not arg))
                                          (buffer-menu arg)))

(define-key ctl-x-map "g"                'goto-line)
(define-key ctl-x-map "m"                '(lambda  ()
                                            "Print manual entry for word at point"
                                            (interactive)
                                            (manual-entry (current-word))))
(define-key ctl-x-map "/"                'query-replace-regexp)
(define-key ctl-x-map "C-v"              'vc-next-action)
(define-key ctl-x-map "C-w"              'write-file) ; ido-write-file is stupid: in fact, need to use C-x C-w C-w

;;;** Misc keybinds
(global-set-key (kbd "M-<delete>")      'flush-lines)
(global-set-key (kbd "C-<delete>")      'keep-lines)
(global-set-key (kbd "M-s")              'isearch-forward-regexp)

; (global-set-key (kbd "S-<iso-lefttab>") 'bh:just-tab) org mode uses this
(global-set-key (kbd "C-<left>")        'backward-word)
(global-set-key (kbd "C-<right>")       'forward-word)
; bigger moves:
(global-set-key (kbd "M-f")             'forward-whitespace)
(global-set-key (kbd "C-S-<right>")     'forward-whitespace)
(global-set-key (kbd "M-b")             '(lambda ()
                                           (interactive) (forward-whitespace '-1)))
(global-set-key (kbd "C-S-<left>")      '(lambda ()
                                           (interactive) (forward-whitespace '-1)))

(global-set-key (kbd "S-<insert>")      'bh:insert-x-selection)
(global-set-key (kbd "C-S-v")           'bh:insert-x-clipboard)

(global-set-key (kbd "C-\\")            'hippie-expand) ; shadows use in company-try-hard
(global-set-key (kbd "C-z")             'undo)
(global-set-key (kbd "C-|")             'repeat-complex-command)
(global-set-key (kbd "C-<return>")      'mark-defun)
(global-set-key (kbd "M->")             'dumb-jump-go)
(global-set-key (kbd "M-<")             'dumb-jump-back)
(global-set-key (kbd "C-@")             'er/expand-region)

; something (what? - dired+ !!! but also ido-menu!!!) stole these:
(global-set-key (kbd "C-t")             'transpose-chars)
(global-set-key (kbd "M-t")             'transpose-words)

; Muscle memory fix - I always forget to use 'e' in buffer-menu and the default
; pushes the selected buffer into the 'next' window - very annoying
(define-key Buffer-menu-mode-map (kbd "<return>") 'Buffer-menu-this-window)

;;;* scroll on mouse wheel

(defun bh:up-slightly ()   (interactive) (scroll-up 5))
(defun bh:down-slightly () (interactive) (scroll-down 5))
(defun bh:up-one ()        (interactive) (scroll-up 1))
(defun bh:down-one ()      (interactive) (scroll-down 1))
(defun bh:up-a-lot ()      (interactive) (scroll-up))
(defun bh:down-a-lot ()    (interactive) (scroll-down))

(global-set-key  (kbd "S-<mouse4>") 'bh:down-one)
(global-set-key  (kbd "S-<mouse5>") 'bh:up-one)
(global-set-key  (kbd "<mouse4>")   'bh:down-slightly)
(global-set-key  (kbd "<mouse5>")   'bh:up-slightly)
(global-set-key  (kbd "C-<mouse4>") 'bh:down-a-lot)
(global-set-key  (kbd "C-<mouse5>") 'bh:up-a-lot)

;; emacs-pgtk
;(global-set-key  (kbd "<wheel-down>")   'scroll-bar-toolkit-scroll)
;(global-set-key  (kbd "<wheel-up>")   'bh:up-slightly)

;;;* GJOTS MODE
; remaining problems:
; you must show-all before saving
; it prompts for coding. default raw-text is OK
(setq format-alist
      (cons '(gjots "gjots" nil "gjots2org" "org2gjots" t nil) format-alist))
(define-derived-mode gjots-mode org-mode "gjots"
  "Major mode for editing gjots files."
  (format-decode-buffer 'gjots)
  (outline-hide-sublevels '1))
;(autoload 'gjots-mode "gjots-mode" "Major mode to edit gjots files." t)
;(setq auto-mode-alist
;      (cons '("\\.gjots$" . gjots-mode) auto-mode-alist))

;;;* Syntax checking on file save:
(defun bh:check-syntax ()
  "Check syntax for various languages."
  (interactive)
  (when (eq major-mode 'lisp-mode)
    (progn
      (emacs-lisp-byte-compile)))
  (when (eq major-mode 'sh-mode)
    (shell-command (format "bash -n %s" buffer-file-name)))
  (when (eq major-mode 'ruby-mode)
    (shell-command (format "ruby -c %s" buffer-file-name)))
  (when (eq major-mode 'python-mode)
    (shell-command (format "python -m py_compile %s" buffer-file-name)))
  (when (eq major-mode 'awk-mode)
    (shell-command (format "gawk --source 'BEGIN { exit(0) } END { exit(0) }' --file %s" buffer-file-name))))

(add-hook 'after-save-hook #'bh:check-syntax)

;;;* buffer management

; from http://mbork.pl/2014-04-04_Fast_buffer_switching_and_friends
(defun bh:switch-bury-or-kill-buffer (&optional aggr)
  "With no argument, switch (but unlike C-x b, without the need
to confirm).  With C-u, bury current buffer.  With double C-u,
kill it (unless it's modified)."
  (interactive "P")
  (cond
   ((eq aggr nil) (progn
                    (cl-dolist (buf '("*Buffer List*" "*Ibuffer*" "*Bookmark List* " "*vc-change-log*" "*Locate*" "*grep*" "*compilation*" ))
                      (when (get-buffer buf)
                        (bury-buffer buf)))
                    (switch-to-buffer (other-buffer))))
   ((equal aggr '(4)) (bury-buffer))
   ((equal aggr '(16)) (kill-buffer-if-not-modified (current-buffer)))))
(global-set-key (kbd "C-`") 'bh:switch-bury-or-kill-buffer)

; better ibuffer format:
(load-file "~/.config/emacs/gk-ibuffer.el")

;;;* projectile
; to make C-c p p report a sorted list of projects:
(defun bh/projectile-sort-projects (projects)
  "Return the sorted list of projects that `projectile-relevant-known-projects'
returned."
  (sort projects 'string<))
(advice-add 'projectile-relevant-known-projects :filter-return #'bh/projectile-sort-projects)

;;;* Misc

(global-auto-revert-mode)

;-------------------------------------
; If line is too long for screen: why isn't this working in doom?
(setq-default truncate-lines t)

;-------------------------------------
; Preserve links to files during backup
(setq-default backup-by-copying-when-linked t)

;-------------------------------------
; Other Emacs variables ...
(column-number-mode 1)

; Not needed in doom:
;(fset 'yes-or-no-p 'y-or-n-p)           ;replace y-e-s by y
;(defalias 'yes-or-no-p 'y-or-n-p)

; Let's try not setting these and see what doom does:
;(setq enable-local-variables t) ; doom has this already
;(setq enable-local-eval t) ; doom is 'maybe' what do I need it for? Long forgotten.
;
(load "vc")
;(define-key vc-prefix-map "=" 'ediff-revision)

;; Visual feedback on selections
;(setq-default transient-mark-mode t)

;; Always end a file with a newline
;;(setq require-final-newline t)
;(setq mode-require-final-newline nil)

;; Stop at the end of the file, not just add lines
;(setq next-line-add-newlines nil)

;; Always spaces, never TABs - doom already has this?
;(setq-default indent-tabs-mode nil)
;(setq default-tab-width 4)

;; experimental: don't popup ediff frames: - doom already has this?
;(setq ediff-window-setup-function #'ediff-setup-windows-plain)

(setq frame-title '("emacs: %b %& %f"))

;doom already has this?
;(setq gdb-many-windows t)

(make-variable-buffer-local 'compile-command)
(setq show-paren-style 'expression)
(show-paren-mode)

; not needed in doom:
;(server-start)

(delete-selection-mode t)
(setq sentence-end "[.?!][]\"')}]*\\($\\|[ \t]\\)[ \t\n]*")
(setq sentence-end-double-space nil)
(set-scroll-bar-mode 'right)
(tool-bar-mode -1) ; no tool bar, thanks
(menu-bar-mode)
(set-default 'compile-command "make -k")
(transient-mark-mode t)
;   (load-file "~/.config/emacs/jkb-compr-ccrypt.el")))
;   (load-file "/usr/share/doc/ccrypt-1.2/jka-compr-ccrypt.el")
;   (require 'jka-compr-ccrypt "jka-compr-ccrypt.el")))

; fix ido-find-file:
; https://superuser.com/questions/312723/how-to-break-out-of-ido-find-files-searching-for
(add-hook 'ido-setup-hook 'shab-ido-config)
(defun shab-ido-config()
 ;; ... other ido-config here ...

 ;; disable auto searching for files unless called explicitly with C-c C-s
  (setq ido-auto-merge-delay-time 99999)
  (define-key ido-file-dir-completion-map (kbd "C-c C-s")
    (lambda()
      (interactive)
      (ido-initiate-auto-merge (current-buffer)))))

(require 'ps-ccrypt "ps-ccrypt.el") ; this only exists in .config/emacs so can't use-package it

; this is built-in: but it works badly with most themes:
(setq global-hl-line-sticky-flag t)
(global-hl-line-mode)
(blink-cursor-mode)

; take off training wheels:
(put 'narrow-to-region 'disabled nil)
(put 'downcase-region 'disabled nil)

; Needed in doom?
;(setq delete-key-deletes-forward t); nope, not needed

; Make Text mode the default mode for new buffers.
(setq default-major-mode 'text-mode)

; Turn on Auto Fill mode automatically in Text mode and related modes.
(add-hook 'text-mode-hook
          (lambda ()
            (auto-fill-mode 1)
            (company-mode -1)
            (smartparens-mode -1)))

(use-package unfill
  ; :ensure ; install in packages.el
  :defer t
  :bind
  ("M-q" . unfill-toggle)
  ("A-q" . unfill-paragraph))

; I think RedHat puts a symbolic link in, Gentoo needs this:
; doom has hunspell ... seems OK
;(setq-default ispell-program-name "aspell")

;; (setq mouse-yank-at-point t) ; nooooooooo!

(windmove-default-keybindings)

; Don't use dialog boxes - Clicking on an install button for instance
; makes Emacs spawn dialog boxes from that point on:
; already in doom
;(setq use-dialog-box nil)

;(setq desktop-path '("~/"))
;(desktop-save-mode 1)

;; so that ido works on insert-char (which has 42k unicode symbols)
;; see https://github.com/DarwinAwardWinner/ido-completing-read-plus/issues/56
(setq ido-cr+-max-items 45000)

(setq makefile-warn-suspicious-lines-p nil)

; let's see what doom does:
;(setq visible-bell t) ; doom has nil which seems to be OK

;(autoload 'new-doc (expand-file-name "~/.config/emacs/new-doc") "Create a new fax.
;Use prefix argument to send a letter instead." t)
;(autoload 'ispell-word "ispell" "Check spelling of word at or before point" t)
;(autoload 'ispell-complete-word "ispell" "Complete word at or before point" t)
;(autoload 'ispell-region "ispell" "Check spelling of every word in the region" t)
;(autoload 'ispell-buffer "ispell" "Check spelling of every word in the buffer" t)
(autoload 'calculate-rectangle "calc-rect" "Calculate sum of numbers in rectangle" t)
;(autoload 'calc             "calc.elc"     "Calculator Mode" t nil)
;(autoload 'calc-extensions  "calc-ext.elc" nil nil nil)
;(autoload 'quick-calc       "calc.elc"     "Quick Calculator" t nil)
;(autoload 'calc-grab-region "calc-ext.elc" nil t nil)
;(autoload 'defmath          "calc-ext.elc" nil t t)
;(autoload 'edit-kbd-macro      "macedit.elc" "Edit Keyboard Macro" t nil)
;(autoload 'edit-last-kbd-macro "macedit.elc" "Edit Keyboard Macro" t nil)
;; (autoload 'c++-mode "c++-mode" "Mode for editing c++" t nil)
;(autoload 'c++-mode  "cc-mode" "C++ Editing Mode" t)
;(autoload 'c-mode    "cc-mode" "C Editing Mode" t)
;(autoload 'objc-mode "cc-mode" "Objective-C Editing Mode" t)
;(autoload 'sgml-mode "sgml-mode" "Major mode to edit SGML files." t)
;(autoload 'xml-mode "sgml-mode" "Major mode to edit XML files." t)
;(autoload 'puppet-mode "puppet-mode" "Major mode to edit puppet files." t)
(setq auto-mode-alist
      (append '(
                ("\\.C$"   . c++-mode)
                ("\\.cpp$" . c++-mode)
                ("\\.cc$"  . c++-mode)
                ("\\.c$"   . c-mode)
                ("\\.h$"   . c-mode)
                ("\\.pp$"  . puppet-mode)
                ("\\.m$"   . objc-mode)) auto-mode-alist))

; Calculator
(global-set-key (kbd "M-#") 'calc)

;;;* bookmark+
;; https://www.emacswiki.org/emacs/BookmarkPlus
;; update with:
;; cd ~/.config/emacs/bookmark+
;; for i in "bookmark+.el" "bookmark+-mac.el" "bookmark+-bmu.el" "bookmark+-key.el" "bookmark+-lit.el" "bookmark+-1.el"; do wget https://www.emacswiki.org/emacs/download/$i; done
(setq load-path (append load-path '("~/.config/emacs/bookmark+")))
(require 'bookmark+)

;;;* browse-kill-ring
;(use-package browse-kill-ring
;  :ensure)

;; browse-kill-ring+
;; https://www.emacswiki.org/emacs/browse-kill-ring+.el
;; update with:
;; cd ~/.config/emacs/browse-kill-ring+
;; for i in "browse-kill-ring+"; rm -f $i; do wget https://www.emacswiki.org/emacs/download/$i; done
; NB seems to break browse-kill-ring-forward and browse-kill-ring-previous
;(setq load-path (append load-path '("~/.config/emacs/browse-kill-ring+")))
;(require 'browse-kill-ring+)

;;;* dired+
; from http://mbork.pl/2015-04-25_Some_Dired_goodies
;(use-package dired+
;  :ensure
;  )
(put 'dired-find-alternate-file 'disabled nil) ; visiting a file from dired closes the dired buffer
; dired - For the few times I’m using Dired, I prefer it not spawning
; an endless amount of buffers. In fact, I’d prefer it using one
; buffer unless another one is explicitly created, but you can’t have
; everything:
(with-eval-after-load 'dired
  (define-key dired-mode-map (kbd "RET") 'dired-find-alternate-file))

;;;* psvn fix
; https://www.emacswiki.org/emacs/SvnStatusMode:
(add-hook 'svn-pre-parse-status-hook 'bh:svn-status-parse-fixup-externals-full-path)
(defun bh:svn-status-parse-fixup-externals-full-path ()
  "Subversion 1.7 adds the full path to externals.  This
pre-parse hook fixes it up to look like pre-1.7, allowing
psvn to continue functioning as normal."
  (goto-char (point-min))
  (let (( search-string  (file-truename default-directory) ))
    (save-match-data
      (save-excursion
        (while (re-search-forward search-string (point-max) t)
          (replace-match "" nil nil))))))

;;;* SRC stuff:
; already in doom:
;(setq find-file-visit-truename t) ;;; follow sym links
(defvar bh:checkin-on-save t)
(defun bh:ensure-in-vc-or-check-in ()
  "Automatically checkin file if it's under the control of SRC.
<file>,v can be in the same directory or in the subdirectory RCS
or .src. The idea is to call vc-checkin only for files not for
buffers."
  (interactive)
  (when (and (bound-and-true-p bh:checkin-on-save) buffer-file-name)
    (setq backend (vc-responsible-backend (buffer-file-name)))
    (when (string= backend 'SRC)
      (if (not backend)
          (vc-register)
       (vc-checkin (list buffer-file-name) backend "auto-checkin" nil)))))

;;;* ripgrep
(grep-apply-setting 'grep-find-command
                    '("rg -n -H --no-heading -e '' $(git rev-parse --show-toplevel 2>/dev/null || pwd)" . 27))
;;;* key-chords

; in ~/.doom.d/packages.el for doom:
;(use-package key-chord
;  :ensure t)
(key-chord-mode 1)
;;
;; and some chords: at least check /usr/share/dict/words
;;

(key-chord-define-global ";'"     'bh:kill-next-window)      ;; C-x 1
;; (key-chord-define-global ",."  'kmacro-end-and-call-macro) ;; C-x e
(key-chord-define-global ",."     "<>\C-b")
(key-chord-define-global "''"     "''\C-b")
(key-chord-define-global "\"\""   "\"\"\C-b")
(key-chord-define-global "<>"     "<>\C-b")
(key-chord-define-global "[]"     "[]\C-b")
(key-chord-define-global "{}"     "{}\C-b")
(key-chord-define-global "()"     "()\C-b")
(key-chord-define-global "xc"     'ibuffer)
(key-chord-define-global "hk"     'describe-key)
(key-chord-define-global "hf"     'describe-function)
(key-chord-define-global "hv"     'describe-variable)

;;;* themes
(add-to-list 'custom-theme-load-path "~/.config/emacs/themes")
(load-theme 'dichromacy-bh t) ; 't' means no check
;;;* org-mode
;;;** electric-indent-mode
; I don't like electric-indent-mode auto-indenting in org-mode. This seems to be
; a solution: https://www.philnewton.net/blog/electric-indent-with-org-mode/
(add-hook 'electric-indent-functions
          (lambda (x) (when (eq 'org-mode major-mode) 'no-indent)))
;;;** org-capture
(setq org-capture-templates
      (quote (("c" "addr" entry (file "~/tmp/addr.org")
"* %?
:PROPERTIES:
:POSITION:
:COMPANY:
:CODE:
:ADDR1:
:ADDR2:
:ADDR3:
:ADDR4:
:TEL:
:FAX:
:XMAS:
:EMAIL:
:NOTES:
:PRINTFLAG:
:SALUTATION:
:DATESTAMP:
:MOBILE:
:END:
"))))

;;;** fixup org-mode's trashing of case-fold-search (actually more likely something in elpa/org-*)
; let's see what doom does with this: seems OK
;(add-hook 'org-mode-hook
;          (lambda () (setq case-fold-search t)))
;;;* doom specifics
;;;** smex
(smex-initialize)
(map! "M-x" 'smex) ; not good enough - we need this in doom:
(define-key! 'override "M-x" 'smex)
(define-key! 'override "M-X" 'smex-major-mode-commands)

;;;** persp
; I don't need this and it doesn't work intuitively anyway
(persp-mode -1)
(map! "C-x b" 'ido-switch-buffer)
;;;** whitespace-mode for doom
; alternate whitespace-mode with whitespace.el defaults, doom defaults and off:

(defun bh:set-whitespace-defaults()
  ; only save the values the first time we get here
  (unless (boundp 'bh:default-whitespace-style)
    ;(append-to-file "bh:set-whitespace-defaults\n" nil "/dev/stdout")
    (setq bh:default-whitespace-style                (default-value 'whitespace-style)
          bh:default-whitespace-display-mappings     (default-value 'whitespace-display-mappings)
          bh:doom-whitespace-style                   whitespace-style
          bh:doom-whitespace-display-mappings        whitespace-display-mappings
          bh:whitespace-mode                         "doom")))

; whitespace-style etc are set up with default-values in whitespace.el and then
; modified in doom-highlight-non-default-indentation-h (in core/core-ui.el).
; This is added to after-change-major-mode-hook in doom-init-ui-h (in
; core/core-ui.el) and called a LOT: so I need to capture doom's modified
; settings after that. The trouble is, this file (config.el) is called _before_
; doom-init-ui-h which is called in window-setup-hook as the last gasp of
; doom-initialize! find-file-hook appears to work.
(add-hook 'find-file-hook #'bh:set-whitespace-defaults 'append)

; doom=>default=>off=>doom=>...
(defun bh:toggle-whitespace () (interactive)
       (cond ((equal bh:whitespace-mode "doom")
              (setq whitespace-style bh:default-whitespace-style
                    whitespace-display-mappings bh:default-whitespace-display-mappings
                    bh:whitespace-mode "default")
              (prin1 (concat "whitespace-mode is whitespace default"))
              (whitespace-mode))
             ((equal bh:whitespace-mode "default")
              (setq bh:whitespace-mode "off")
              (prin1 (concat "whitespace-mode is off"))
              (whitespace-mode -1))
             (t ; (equal bh:whitespace-mode "off")
              (setq whitespace-style bh:doom-whitespace-style
                    whitespace-display-mappings bh:doom-whitespace-display-mappings
                    bh:whitespace-mode "doom")
              (prin1 (concat "whitespace-mode is doom default"))
              (whitespace-mode))))
(global-set-key (kbd "C-<f4>")          'bh:toggle-whitespace)

;;;** Nah! I disagree with doom on these:
(remove-hook 'doom-first-buffer-hook #'smartparens-global-mode)
(defun bh:defeat-doom-malignancies()
; smartparens-mode is soooo annoying!
; also the keychords for '()' '[]' etc are more useful

; BTW why, oh why would these be set to sp-backward-slurp-sexp etc for godsake, even
; outside elisp mode! But smartparens is hard-coded into core doom so we can
; only turn it off:
;  (define-key smartparens-mode-map (kbd "C-<right>") nil)
;  (define-key smartparens-mode-map (kbd "C-<left>") nil)
;  (define-key smartparens-mode-map (kbd "M-<right>") nil)
;  (define-key smartparens-mode-map (kbd "M-<left>") nil)
  (smartparens-mode -1) ; this was fixed on 20201102 but leave this here in case
                        ; he decides to enable them in lisp-mode!

; company is also very annoying and unhelpful. turn it off initially.
; hippie-expand works fine
  (company-mode -1)

  ; doom is just too clever with org mode:
  (setq org-hide-leading-stars nil)
  (org-indent-mode -1))
(add-hook 'find-file-hook #'bh:defeat-doom-malignancies 'append)

(define-key ctl-x-map "g"                'goto-line)
(setq tab-always-indent t)
(setq tabify-regexp " [ \t]+")

;;;* outline-minor-mode
;; This makes outline-minor-mode operate a bit more like org-mode <tab> & S-<tab>
;; ... but I still want indent on <tab>, so I'm using C-<tab> for show/hide!!

; now in ~/.doom.d/packages.el:
;(use-package outline-magic
;  :ensure t)

; this enables C-c C-c to be used instead of the very awkward C-c C-@ prefix
(add-hook 'outline-minor-mode-hook (lambda ()
                                     (local-set-key "\C-c\C-c"
                                                    outline-mode-prefix-map)))
(add-hook 'outline-minor-mode-hook
          (lambda ()
            (define-key outline-minor-mode-map (kbd "C-<f12>") 'outline-cycle)
            (define-key outline-minor-mode-map (kbd "C-<tab>") 'outline-cycle)
            (define-key outline-minor-mode-map (kbd "C-c C-n") 'outline-next-visible-heading)
            (define-key outline-minor-mode-map (kbd "C-c C-p") 'outline-previous-visible-heading)
            (define-key outline-minor-mode-map (kbd "C-c C-f") 'outline-forward-same-level)
            (define-key outline-minor-mode-map (kbd "C-c C-b") 'outline-backward-same-level)
            (define-key outline-minor-mode-map (kbd "S-<iso-lefttab>")
              '(lambda()
                 (interactive)
                 (save-excursion
                   (goto-char (point-min))
                   (outline-show-all)
                   (outline-hide-leaves))))))

;;;* Final

; recentf list only gets saved on graceful exit - so it does not get
; saved on abnormal exit or when running emacs server. But during
; startup a number of files gets visited, whether you have
; desktop-save-mode or not and it slows startup a lot. So delay adding
; the hook to the last possible moment.
(defun bh:recentf-mode-startup ()
  (recentf-mode 1)
  (add-hook 'find-file-hook #'recentf-save-list))
(add-hook 'desktop-after-read-hook #'bh:recentf-mode-startup)

;;;* Startup for this file:

;;;* Local variables
;; Local Variables:
;; outline-regexp: ";;;\\*+\\|\\`"
;; eval: (outline-minor-mode 1)
;; eval: (outline-hide-sublevels 4)
;; End:
