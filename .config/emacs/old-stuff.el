;; -*- Lisp -*-

(defun ansi-frame ()
  "Convert old c function header to ANSI. Position at the { before running."
  (interactive)
  (let ((beg (point))
	(lbrace (point)))
    (insert-string "~")
    (search-backward ";")		; replace final ; in arg list with ")"
    (delete-char 1)
    (insert-string ")")
    (setq beg (point))			; at the new ")"
    (search-backward "(")		; find "(...)" and delete it
    (setq lbrace (point))
    (search-forward ")")
    (forward-char 2)			; also delete the eol
    (delete-region lbrace (point))
    (insert-string " ARG1(")
    (delete-horizontal-space)
    (setq lbrace (point))
    (search-forward "~")
    (delete-char -1)
    (setq beg (point))
    (goto-char lbrace)
    (narrow-to-region lbrace beg)
    (goto-char (point-min))
    (replace-regexp "^[ 	]*" "")	; delete leading spaces
    (goto-char (point-min))
    (while (search-forward "," nil t)
      (beginning-of-line nil)
    ; "\\1 \\2, \\3" to "\\1 \\2;<nl>\\1 \\2" ...
      (replace-regexp "^\\([^,]+\\)[ 	]+\\([^,]+\\),[ 	]*\\(.*\\)$" 
		      "\\1 \\2;\n\\1 \\3")				
      (goto-char 0))
    (goto-char (point-min))
    (replace-regexp ";[ 	]*" ","); change ; to ,
    (goto-char (point-min))
    (replace-regexp "[ 	]*\\*\\([^\\*]*\\)" " *	\\1") ; c *p to c * p
    (goto-char (point-min))
    (replace-regexp "[ 	]+\\([^ 	]+\\)$" ",\\1")	; spaces to ,
    (goto-char (point-min))
    (replace-regexp "," ",	")
    (widen)
    (indent-region lbrace (point) nil)
    (search-backward "(")))

(defun new-indents () "Convert leading tabs to 4 spaces" (interactive)
  (tabify (point-min) (point-max))
  (goto-char (point-min))
  (replace-regexp "^\t\t\t\t\t\t\t\t" "                                " nil)
  (goto-char (point-min))
  (replace-regexp "^\t\t\t\t\t\t\t" "                            " nil)
  (goto-char (point-min))
  (replace-regexp "^\t\t\t\t\t\t" "                        " nil)
  (goto-char (point-min))
  (replace-regexp "^\t\t\t\t\t" "                    " nil)
  (goto-char (point-min))
  (replace-regexp "^\t\t\t\t" "                " nil)
  (goto-char (point-min))
  (replace-regexp "^\t\t\t" "            " nil)
  (goto-char (point-min))
  (replace-regexp "^\t\t" "        " nil)
  (goto-char (point-min))
  (replace-regexp "^\t" "    " nil))

(defun reformat-c () "Re-format c code eliminating } on their own lines"
  (interactive)
  (beginning-of-buffer nil)
  (replace-regexp "[ 	]+$" "" nil)
  (beginning-of-buffer nil)
  (replace-regexp "\n\n{" "\n{" nil)
  (beginning-of-buffer nil)
  (replace-regexp "\\(.\\)\n[ 	]+{" "\\1 {" nil)
  (beginning-of-buffer nil)
  (replace-regexp "}\n[ 	]+else" "} else" nil)
  (beginning-of-buffer nil)
  (replace-regexp "}\n[ 	]+while" "} while" nil)
)

(put 'upcase-region 'disabled nil)

(defun c-remove-comment-box ()
  "Removes /*** .... ***/ lines as well as ' * ' strings."
  (interactive)
  (search-backward "/****")
  (kill-whole-line)
  (set-mark (point))
  (search-forward "****/")
  (kill-whole-line)
  (narrow-to-region (mark) (point))
  (goto-char (point-min))
  (replace-regexp "^ +\\* +" "")
  (goto-char (point-min))
  (replace-regexp "[ 	]*\\*[ 	]*$" ""))

(defun c-add-comment-box ()
  "Place all text in a box. Assumes we are narrowed on the comment."
  (interactive)
  (let (old-case-fold case-fold-search)
    (untabify (point-min) (point-max))
    (goto-char (point-min))
    (replace-regexp "^" " * ")
    (goto-char (point-min))
    (replace-regexp "$" "                                                                                ")
    (goto-char (point-min))
    (setq case-fold-search nil)
    (replace-regexp "^\\(..........................................................................\\) *$" "\\1*")
    (setq case-fold-search old-case-fold)
    (goto-char (point-min))
    (insert-string "/**************************************************************************\n")
    (goto-char (point-max))
    (insert-string "\n **************************************************************************/\n")
    (widen)))

(gnuserv-start nil)

(setq update-time-stamp-string "This page last updated on ")
(defun update-time-stamp ()
  "Looks for the string \"update-time-stamp-string\" and replaces the rest of the line with a timestamp"
  (interactive)
  (save-excursion ;;; push emacs status on a stack to be restored on exit
    (and update-time-stamp-string ;;; make sure it's defined
	 (goto-char (point-min)) ;;; goto start of file
	 (search-forward update-time-stamp-string) 
	 (progn (kill-line) 
		(time-stamp))))) ;;; do the deed

(defun prep-fax () "Prepare a fax message in current buffer." 
  (interactive)
  (setq fill-column 80)
  (insert "\n[Susanna - this is printing on 29/F - could you please send it? Thanks, Bob.]")
  (insert "\n\n              --- F A X  M E S S A G E ---\n")
  (insert "\nTo  : \nFrom: Bob Hepple\nDate: " (current-time-string) "\nRe  : \nCC  : ")
  (previous-line 4)
  (end-of-line nil))

(setq sendmail-program "mail-prog") ; Hack to save mail to ~/mqueue until PPP is up

(defun print-region-as-fax () 
  "Format the current region into a Postscript fax." 
  (interactive)
  (save-excursion
    (setq fill-column 70)
    (setq beg (point))
    (setq end (mark))
    (if (< end beg)
	(progn 
	  (setq beg (mark)) 
	  (setq end (point))))
    (untabify beg end)
    (message "Composing fax...")
    (write-region beg end "/tmp/junk" nil 'quiet)
    (shell-command-on-region beg end "prep_fax /tmp/junk" nil)
    (shell-command "rm /tmp/junk" nil)
    (message "Composing fax...done")
    ))

(defun print-banner-on ( )
  "Enable printing banner for print-buffer, print-region etcetera"
  (interactive)
  (message "Burst page ON")
  (setq lpr-switches (delq '("-h") lpr-switches)))

(defun print-banner-off ( )
  "Disable printing banner for print-buffer, print-region etcetera"
  (interactive)
  (message "Burst page OFF")
  (setq lpr-switches (concat "-h" lpr-switches)))

(defun toggle-burst-page ()
  "Toggle printing of burst page - not working!"
  (interactive)
  (if (assoc '("-h") lpr-switches)
      (print-banner-on)
    (print-banner-off)))

(defun toggle-printer ()
  "Toggle printer between lp to lp2"
  (interactive)
  (if (string-match "-Plp2" lpr-switches)
      (progn ())
    (progn (setq lpr-switches (concat "-Plp2" lpr-switches))
	   (message "Printer set to lp2"))))

(defun print-postscript () "Send buffer to printer without burst page" 
  (interactive)
  (print-banner-off)
  (lpr-buffer)
  (print-banner-on))
