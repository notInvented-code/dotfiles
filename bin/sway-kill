#!/usr/bin/env bash

# Copyright (C) 2020-2021 Bob Hepple <bob.hepple@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# http://bhepple.freeshell.org

[[ "$1" ]] && {
    echo "$( basename $0 ): kill the focused program"
    exit 0
}

PID=$(swaymsg -t get_tree | jq '.. | select(.type?) | select(.focused==true).pid')

if [[ "$PID" ]]; then
    kill $PID
else
    swaynag -m "$0: No PID was obtained"
fi
